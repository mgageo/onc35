# <!-- coding: utf-8 -->
#
# la partie cartes
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# la totale
# source("geo/scripts/onc35.R");cartes_jour();
cartes_jour <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  drive_jour()
  ppe_jour(force=TRUE)
  cartes_parcours_a4_jour()
  cartes_parcours_jour()
  cartes_points_jour()
}
#
# production des cartes suite à une mise à jour des sites
#
#
# source("geo/scripts/onc35.R");cartes_parcours_jour();
cartes_parcours_jour <- function(test=2) {
  carp()
  library(tidyverse)
  library(janitor)
  couches_parcours_jour()

#  stop('***')
  cartes_parcours_a3(test=test)
  tex_pdflatex('parcours_a3.tex')
  cartes_parcours_a3_pdf()
}
#
# source("geo/scripts/onc35.R");cartes_parcours_a4_jour();
cartes_parcours_a4_jour <- function(test=2) {
  carp()
  library(tidyverse)
  library(janitor)
  couches_parcours_jour()
  cartes_parcours_a4(test=test)
  tex_pdflatex('parcours.tex')
}
#
# source("geo/scripts/onc35.R");cartes_points_jour();
cartes_points_jour <- function(test=2) {
  carp()
  library(tidyverse)
  library(janitor)
  couches_points_ecrire()
  cartes_points_cartes(test=test)
  tex_pdflatex('points.tex')
  cartes_points_pdf()
}
#
# les cartes pour l'ensemble des sites, la version Tex
# source("geo/scripts/onc35.R"); cartes_carres_tex()
cartes_carres_tex <- function(test=1) {
  carp()
  library(tidyverse)
  library(utf8)
  library(janitor)
  texFic <- sprintf("%s/%s", texDir, "cartes_carres.tex")
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
  cartesDir <- sprintf("%s/cartes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
# http://www.tablesgenerator.com/
#
# le template tex
  dsn <- sprintf("%s/cartes_carres_tpl.tex", texDir)
  template <- readLines(dsn)
  carres.sf <- couches_carres_lire()
  carres.df <- st_set_geometry(carres.sf, NULL)
#  View(carres.df) ; stop("***")
#  test <- 2
  for ( i in seq_along(carres.sf) ) {
    if ( i > 4 ) {
#      break
    }
    sf1 <- carres.sf[i,]
    carres <- carres.df[i, 'id']
    carresDir <- sprintf("%s/carres/%s", texDir, carres)
    carp('carresDir: %s', carresDir)
    dir.create(carresDir, showWarnings = FALSE, recursive = TRUE)
    tpl <- template
    tpl <- tex_df2tpl(carres.df, i, tpl)
    tex <- append(tex, tpl)

  }
  write(tex, file = TEX, append = FALSE)
  carp("texFic: %s", texFic)
}
#
# génération des différentes cartes pour les carrés
# source("geo/scripts/onc35.R"); cartes_carres_cartes()
cartes_carres_cartes <- function(test=2) {
  carp()
  library(tidyverse)
  carres.sf <<- ppe_carres_lire()
  points.sf <<- ppe_points_lire()
  cercles.sf <<- st_buffer(points.sf, 100)
#  points_carte.sf <<- st_buffer(points.sf, 150, nQuadSegs = 1, endCapStyle='SQUARE')
  points_carte.sf <<- st_buffer(points.sf, 150)
  carres.df <- st_set_geometry(carres.sf, NULL)
  cartesDir <- sprintf("%s/cartes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
  for ( i in 1:nrow(carres.sf) ) {
    if ( i > 1 ) {
#      break
    }
    sf1 <- carres.sf[i,]
    cartes_carre_cartes(sf1, 'sc1000', lwd=20, test=test)
    cartes_carre_cartes(sf1, 'carte', lwd=3, marge=150, test=test)
    cartes_carre_cartes(sf1, 'gb_photo', lwd=3, marge=150, test=test)
    cartes_a4_cartes(sf1, 'gb_photo', lwd=3, marge=150, test=test)
#    cartes_carre_cartes(sf1, 'osm_tous_fr', lwd=8, marge=150, test=test)
  }
}
#
# génération d'une carte
cartes_carre_cartes <- function(nc, couche='sc1000', lwd=3, test=2, marge=0) {
  library(raster)
  cartesDir <- sprintf("%s/cartes", texDir)
  dsn <- sprintf('%s/%s_%s.pdf', cartesDir, nc$id[1], couche)
  if (file.exists(dsn) & test>1) {
    return()
  }
  carp('dsn: %s', dsn)
#  return()
  rasterFic <- sprintf('%s/ogc/%s_%s.tif', varDir, nc$id[1], couche)
  carp("rasterFic: %s", rasterFic)
#  stop('***')
  img <- brick(rasterFic)
  if ( marge > 0 ) {
    e <- fonds_extent(nc, marge)
    img <- crop(img, e)
  }
#  img1 <- projectRaster(img, crs=CRS("+init=epsg:2154"))
  plotImg(img)
  plot(st_geometry(nc), lwd=lwd, border='darkred', add=TRUE)
  text(st_coordinates(st_centroid(points.sf)), labels = points.sf$point, cex=2, col='black')
  plot(st_geometry(points_carte.sf), lwd=3, border='black', add=TRUE)
#  scalebar(500, type='bar', divs=2)

#  mga.img <<- img
#  mga.sf <<- nc
#  stop('***')
  dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
  graphics.off()
}
#
# génération de 4 a4 pour un carré
cartes_a4_cartes <- function(nc, couche='sc1000', lwd=3, test=2, marge=0) {
  carp()
  cartesDir <- sprintf("%s/cartes", texDir)
  bb <- st_bbox(nc) %>%
    glimpse()
  largeur <- bb[3] - bb[1]
  hauteur <- bb[4] - bb[2]
  a4.df <- data.frame(
    id=c('NW', 'NE', 'SE', 'SW'),
    x=c(bb[1]+.25*largeur, bb[1]+.75*largeur, bb[1]+.75*largeur, bb[1]+.25*largeur),
    y=c(bb[2]+.75*hauteur, bb[2]+.75*hauteur, bb[2]+.25*hauteur, bb[2]+.25*hauteur)
  ) %>%
    glimpse()
  a4.sf <- st_as_sf(a4.df, coords=c("x", "y"), crs = 2154) %>%
    glimpse()
# vérification du bon positionnement des points
#  plot(st_geometry(nc))
#  plot(st_geometry(a4.sf),add=TRUE)
#  text(st_coordinates(st_centroid(a4.sf)), labels = a4.sf$id, cex=2, col='black')
  rasterFic <- sprintf('%s/ogc/%s_%s.tif', varDir, nc$id[1], couche)
  carp("rasterFic: %s", rasterFic)
  img <- brick(rasterFic)
  for ( i in 1:nrow(a4.sf) ) {
    cartes_a4_carte(a4.sf[i,], img, lwd=3, marge=300, test=test)
    dsn <- sprintf('%s/%s_%s_%s.pdf', cartesDir, nc$id[1], a4.sf$id[i], couche)
    dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
    graphics.off()
  }
}

#
# génération d'un a4
cartes_a4_carte <- function(nc, img, lwd=3, test=2, marge=0) {
  carp()
  if ( marge > 0 ) {
    e <- fonds_extent(nc, marge)
    img <- crop(img, e)
  }
  plotImg(img, alpha=120)
  plot(st_geometry(carres.sf), lwd=lwd, border='darkred', add=TRUE)
  text(st_coordinates(st_centroid(points.sf)), labels = points.sf$point, cex=2, col='black')
  plot(st_geometry(cercles.sf), lwd=lwd, border='black', add=TRUE)
}


#
# génération de la carte du département
# source("geo/scripts/onc35.R"); cartes_departement()
cartes_departement <- function(test=1) {
  library(tidyverse)
  nc <- couches_ppe_lire() %>%
    filter(! st_geometry_type(.) %in% c("POINT") ) %>%
    glimpse()
  parcours.df <- drive_observateurs_lire('parcours')
  nc <- nc %>%
    left_join(parcours.df, by=c('id'='parcours')) %>%
    filter(pdf=='oui') %>%
    mutate(numero=seq.int(nrow(.))) %>%
    dplyr::select(numero, id, prenom) %>%
    glimpse()
  df <- nc %>%
    st_set_geometry(NULL)
  print(knitr::kable(df))
  cartes_departement_carres(nc)
  stop('***')
  observateurs.df <- drive_observateurs_lire()
  df <- nc %>%
    st_set_geometry(NULL) %>%
    left_join(observateurs.df, by=c('email')) %>%
    dplyr::select(numero, carre, prenom) %>%
    print(n=20, na.print='')
  tex_df2table(df, suffixe='liste')
  df <- nc %>%
    st_set_geometry(NULL)
  ocs.df <- ocs_cesbio_carres_stat_lire()
#
# le template tex
  dsn <- sprintf("%s/cartes_departement_tpl.tex", texDir)
  template <- readLines(dsn)
  texFic <- sprintf("%s/%s", texDir, "cartes_departement.tex")
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
  for ( i in 1:nrow(nc) ) {
    if ( i > 4 ) {
#      break
    }
    nc1 <- nc[i,]
    cartes_carre_cartes(nc1, 'carte', lwd=3, marge=150, test=test)
    tpl <- template
    tpl <- tex_df2tpl(df, i, tpl)
    tex <- append(tex, tpl)
  }
  write(tex, file = TEX, append = FALSE)
  carp(" texFic: %s", texFic)
}
# source("geo/scripts/onc35.R"); cartes_dpt35_ens()
cartes_dpt35_ens <- function(test=1) {
  library(tidyverse)
  library(raster)
  nc <- couches_ens_lire() %>%
    glimpse()
  cartesDir <- sprintf("%s/cartes", texDir)
  dsn <- sprintf('%s/dpt35_ens.pdf', cartesDir)
  img <- couches_departement_lire()
  plotImg(img)
  plot(st_geometry(nc), border='darkblue', lwd=5, add=TRUE)
  nc <- nc %>%
    filter(ID_SITE %in% c('125', '109', '128')) %>%
    glimpse()
  plot(st_geometry(nc), border='red', lwd=5, add=TRUE)
#  scalebar(200, type='bar', divs=2)
  dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
}
# source("geo/scripts/onc35.R"); cartes_dpt35_paysages()
cartes_dpt35_paysages <- function(test=1) {
  library(tidyverse)
  library(raster)
  nc <- ocs_paysages_wfs_lire()
  img <- ocs_paysages_raster_lire()
  plotImg(img)
  plot(st_geometry(nc), border='black', lwd=5, add=TRUE)
#  scalebar(200, type='bar', divs=2)
  cartesDir <- sprintf("%s/cartes", texDir)
  dsn <- sprintf('%s/dpt35_paysages.pdf', cartesDir)
  dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
}
#
#
cartes_departement_carres <- function(nc) {
  library(raster)
  carp()
  cartesDir <- sprintf("%s/cartes", texDir)
  dsn <- sprintf('%s/dpt35.pdf', cartesDir)
  img <- couches_departement_lire()
  plotImg(img)
  text(st_coordinates(st_centroid(nc)), labels = nc$numero, cex=2, col='darkblue')
#  scalebar(200, type='bar', divs=2)
  dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
  carp('dsn: %s', dsn)
#  graphics.off()
}
#
# génération de la carte du département pour les parcours
# source("geo/scripts/onc35.R"); cartes_departement_parcours()
cartes_departement_parcours <- function(test=2) {
  carp()
  parcours.df <- drive_parcours_lire()
  nc <- ppe_parcours_lire() %>%
    left_join(parcours.df, by=c('dsn'='parcours')) %>%
    glimpse() %>%
    print(n=30)
#  st_agr(nc) = "constant"
  nc$numero <-  seq.int(nrow(nc))
  points.sf <<- ppe_points_lire()
#  stop('***')
  cartes_departement_parcours_carte(nc)
  cartes_departement_parcours_liste(nc)
}
# la carte du département pour les parcours
cartes_departement_parcours_carte <- function(nc) {
  library(raster)
  cartesDir <- sprintf("%s/cartes", texDir)
  dsn <- sprintf('%s/cartes_departement_parcours.pdf', cartesDir)
  img <- couches_departement_lire()
  plotImg(img)
  text(st_coordinates(st_centroid(nc)), labels = nc$numero, cex=2, col='red')
  scalebar(200, type='bar', divs=2)
  dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
  graphics.off()
  carp('dsn: %s', dsn)
}
cartes_departement_parcours_liste <- function(nc) {
  carp()
  df <- nc %>%
    st_transform(2154) %>%
    mutate(lg= as.integer(st_length(.))) %>%
    st_buffer(., 200) %>%
    mutate(surface=as.integer(st_area(.)/10000)) %>%
    st_set_geometry(NULL) %>%
    glimpse() %>%
    dplyr::select(no=numero, parcours=dsn, prenom, lg, ha=surface) %>%
    print(n=20, na.print='')
  tex_df2table(df)
}



#
# génération des deux cartes a3
# source("geo/scripts/onc35.R"); cartes_parcours_a3()
cartes_parcours_a3 <- function(test=1) {
  carp()
  library(raster)
  parcours.df <- drive_parcours_lire()
  nc <- ppe_parcours_lire() %>%
    left_join(parcours.df, by=c('dsn'='parcours')) %>%
#    filter(pdf=='oui') %>%
#    filter(grepl('Tinten', id)) %>%
    glimpse() %>%
    print(n=30)
#  stop('***')
  nc$numero <-  seq.int(nrow(nc))
  points.sf <<- ppe_points_lire()
#
# le template tex
  dsn <- sprintf("%s/cartes_parcours_a3_tpl.tex", texDir)
  template <- readLines(dsn)
  texFic <- sprintf("%s/%s", texDir, "cartes_parcours_a3.tex")
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
  df <- nc %>%
    st_set_geometry(NULL)
  for ( i in 1:nrow(nc) ) {
    if ( i > 4 ) {
#      break
    }
    nc1 <- nc[i,]
#    cartes_parcours_a3_cartes(nc1, 'gb_photo', lwd=3, marge=250, test=test, alpha=120, maxpixels=10000000)
    cartes_parcours_a2_carte(nc1, 'gb_photo', lwd=3, marge=250, test=test, gris=120, maxpixels=10000000)
    cartes_parcours_a2_carte(nc1, 'gb_photo', lwd=3, marge=250, test=test, alpha=120, maxpixels=10000000)
    tpl <- template
    tpl <- tex_df2tpl(df, i, tpl)
    tex <- append(tex, tpl)
#    stop('***')
  }
  write(tex, file = TEX, append = FALSE)
  carp(" texFic: %s", texFic)
}
#
# découpe du pdf
# source("geo/scripts/onc35.R");cartes_parcours_a3_pdf();
cartes_parcours_a3_pdf <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  parcours.df <- drive_parcours_lire()
  nb_pages <- 2
  debut_page <- 1
  df <- ppe_parcours_lire() %>%
    left_join(parcours.df, by=c('dsn'='parcours')) %>%
#    filter(pdf=='oui') %>%
    st_set_geometry(NULL) %>%
    group_by(dsn, prenom) %>%
    summarize(nb=n()) %>%
    ungroup() %>%
    mutate(fichier=sprintf("%s_parcours_a3", dsn)) %>%
    mutate(id=1:nrow(.), id=id-1) %>%
    mutate(premiere=id*nb_pages+debut_page) %>%
    mutate(derniere=(id+1)*nb_pages+debut_page-1) %>%
    glimpse() %>%
    dplyr::select(dossier=prenom, fichier, premiere, derniere) %>%
    glimpse()
#  stop('***')
  sedja_decoupe_sh(dossier='ONC35', fichier='parcours_a3', df)
}
#
# génération du a2
cartes_parcours_a2_carte <- function(nc, couche='sc1000', lwd=3, test=2, marge=0, maxpixels=50000, alpha=120, gris=0) {
  carp()
  cartesDir <- sprintf("%s/cartes_parcours", texDir)
  parcours <- sprintf('%s', nc$id[1])
  if ( gris == 0 ) {
    fic <- sprintf('%s_%s_couleur', parcours, couche)
  } else {
    fic <- sprintf('%s_%s_gris', parcours, couche)
  }
  dsn <- sprintf('%s/%s.png', cartesDir, fic)
  if (file.exists(dsn) & test>1) {
    return()
  }
  rasterFic <- sprintf('%s/ogc/%s_%s.tif', varDir, nc$id[1], couche)
  carp("rasterFic: %s", rasterFic)
  if ( gris == 0 ) {
    img <- brick(rasterFic)
  } else {
    img <- raster(rasterFic)
  }
  if ( marge > 0 ) {
    e <- fonds_extent(nc, marge)
    img <- crop(img, e)
  }
  carp("ncols=%s nrows:%s", img@ncols, img@nrows)
  if ( gris == 0 ) {
    plotImg(img, alpha=alpha, png=dsn)
  } else {
    plotImgGray(img, alpha=gris, png=dsn)
  }

  plot(st_geometry(nc), add=TRUE, border='red', lwd=5)
  text(st_coordinates(st_centroid(points.sf)), labels = points.sf$point, cex=2, col='black')
  scalebar(200, type='bar', divs=2)
  north("topleft", cex=2)
  north("topright", cex=2)
  north("bottomleft", cex=2)
  north("bottomright", cex=2)
  carp("width=%s height:%s", par("din")[1], par("din")[2])
#  dev.copy(png, dsn, width = img@ncols, height = img@nrows, units = "px", pointsize = 12)
#  graphics.off()
  dev.off()
  cartes_parcours_a2_2a3(fic)
#  stop('***')
}
#
# découpe du a2 en deux a3
cartes_parcours_a2_2a3 <- function(fic) {
#  carp()
  library(magick)
  cartesDir <- sprintf("%s/cartes_parcours", texDir)
  dsn <- sprintf('%s/%s.png', cartesDir, fic)
  a2 <- image_read(dsn)
  info <- image_info(a2)
  largeur <- info$width
  hauteur <- info$height
  if ( largeur < hauteur) {
    a2 <- image_rotate(a2, 90)
    info <- image_info(a2)
  }
  largeur <- floor(info$width/2)
  hauteur <- info$height
  gauche <- largeur
  haut <- 0
  crop <- sprintf("%dx%d+%d+%d", largeur, hauteur, gauche, haut)
  carp('fic: %s crop:%s', fic, crop)
  a3_droite <- image_crop(a2, crop)
  largeur <- floor(info$width/2)
  hauteur <- info$height
  gauche <- 0
  haut <- 0
  crop <- sprintf("%dx%d+%d+%d", largeur, hauteur, gauche, haut)
  a3_gauche <- image_crop(a2, crop)
  dsn <- sprintf('%s/%s_droite.pdf', cartesDir, fic)
  image_write(a3_droite, path = dsn, format = "pdf")
  dsn <- sprintf('%s/%s_gauche.pdf', cartesDir, fic)
  image_write(a3_gauche, path = dsn, format = "pdf")
#  carp('dsn:%s', dsn)
}
#
# génération de la carte du département pour les parcours
# source("geo/scripts/onc35.R"); cartes_parcours_a4()
cartes_parcours_a4 <- function(test=2) {
  carp()
  parcours.df <- drive_parcours_lire()
  nc <- ppe_parcours_lire() %>%
    left_join(parcours.df, by=c('dsn'='parcours')) %>%
    glimpse() %>%
    print(n=30)
#  st_agr(nc) = "constant"
  nc$numero <-  seq.int(nrow(nc))
  points.sf <<- ppe_points_lire() %>%
    glimpse()
  cartes_departement_parcours(nc)
  cartes_departement_parcours_liste(nc)
#  return()
  df <- nc %>%
    st_set_geometry(NULL)
#
# le template tex
  dsn <- sprintf("%s/cartes_parcours_a4_tpl.tex", texDir)
  template <- readLines(dsn)
  texFic <- sprintf("%s/%s", texDir, "cartes_parcours_a4.tex")
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
  for ( i in 1:nrow(nc) ) {
    if ( i > 4 ) {
#      break
    }
    nc1 <- nc[i,]
    cartes_parcours_a4_carte(nc1, 'gb_scan', lwd=3, marge=250, test=test, maxpixels=1000000)
    cartes_parcours_a4_carte(nc1, 'gb_photo', lwd=3, marge=250, test=test, maxpixels=1000000)
    cartes_parcours_a4_carte(nc1, 'sc1000', lwd=3, marge=20000, test=test)
    tpl <- template
    tpl <- tex_df2tpl(df, i, tpl)
    tex <- append(tex, tpl)
#    stop('***')
  }
  write(tex, file = TEX, append = FALSE)
  carp("texFic: %s", texFic)
}
#
# génération d'une carte parcours
cartes_parcours_a4_carte <- function(nc, couche='sc1000', lwd=3, test=1, marge=0, alpha=255, maxpixels=5000000) {
  library(raster)
  carp()
  cartesDir <- sprintf("%s/cartes", texDir)
  parcours <- sprintf('%s', nc$id[1])
  dsn <- sprintf('%s/%s_%s.pdf', cartesDir, parcours, couche)
  if (file.exists(dsn) & test>1) {
    return()
  }
  carp('dsn: %s', dsn)
#  return()
  rasterFic <- sprintf('%s/ogc/%s_%s.tif', varDir, parcours, couche)
  carp("rasterFic: %s", rasterFic)
#  stop('***')
  img <- brick(rasterFic)
  if ( marge > 0 ) {
    e <- fonds_extent(nc, marge)
    img <- crop(img, e)
  }
#  img1 <- projectRaster(img, crs=CRS("+init=epsg:2154"))
  plotImg(img, alpha=alpha, maxpixels=maxpixels)
  plot(st_geometry(nc), add=TRUE, border='red', lwd=5)
  nc1 <- st_buffer(nc, 200)
  plot(st_geometry(nc1), add=TRUE, border='orange', lwd=3)
#  glimpse(points.sf); stop('***')
  nc2 <- points.sf %>%
    glimpse() %>%
    filter(carre == !!parcours)
  if(nrow(nc2) == 0) {
    View(points.sf)
    die('pas de points, parcours: %s', parcours)
  }
  text(st_coordinates(st_centroid(nc2)), labels = nc2$point, cex=2, col='red')
  nc3 <- st_buffer(nc2, 200)
  plot(st_geometry(nc3), add=TRUE, border='red', lwd=2)
  scalebar(200, type='bar', divs=2)
  dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
  graphics.off()
}
# découpe du pdf
# source("geo/scripts/onc35.R");cartes_parcours_a4_pdf();
cartes_parcours_a4_pdf <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  parcours.df <- drive_parcours_lire()
  nb_pages <- 2
  debut_page <- 3
  df <- ppe_parcours_lire() %>%
    left_join(parcours.df, by=c('dsn'='parcours')) %>%
#    filter(pdf=='oui') %>%
    st_set_geometry(NULL) %>%
    group_by(dsn, prenom) %>%
    summarize(nb=n()) %>%
    ungroup() %>%
    mutate(fichier=sprintf("%s_parcours_a4", dsn)) %>%
    mutate(id=1:nrow(.), id=id-1) %>%
    mutate(premiere=id*nb_pages+debut_page) %>%
    mutate(derniere=(id+1)*nb_pages+debut_page-1) %>%
    glimpse() %>%
    dplyr::select(dossier=prenom, fichier, premiere, derniere) %>%
    glimpse()
#  stop('***')
  sedja_decoupe_sh(dossier='ONC35', fichier='parcours', df)
}
#
# génération des cartes ocs pour les parcours
# source("geo/scripts/onc35.R"); cartes_parcours_ocs()
cartes_parcours_ocs <- function(test=2) {
  carp()
  nc <- couches_parcours_lire()
  df <- nc %>%
    st_set_geometry(NULL)
#
# le template tex
  dsn <- sprintf("%s/cartes_parcours_ocs_tpl.tex", texDir)
  template <- readLines(dsn)
  texFic <- sprintf("%s/%s", texDir, "cartes_parcours_ocs.tex")
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
  for ( i in 1:nrow(nc) ) {
    if ( i > 4 ) {
#      break
    }
    nc1 <- nc[i,]
    cartes_parcours_cartes(nc1, 'satellite', lwd=3, marge=250, test=test, maxpixels=100000)
    tpl <- template
    tpl <- tex_df2tpl(df, i, tpl)
    tex <- append(tex, tpl)
  }
  write(tex, file = TEX, append = FALSE)
  carp(" texFic: %s", texFic)
  tex_pdflatex('parcours_ocs.tex')
}
#
# pour les points
# ========================================================================
#
# génération des différentes cartes pour les points
# source("geo/scripts/onc35.R"); cartes_points_cartes()
cartes_points_cartes <- function(test=2) {
  carp()
  library(tidyverse)
  parcours.df <- drive_parcours_lire()
  nc <- ppe_points_lire() %>%
    left_join(parcours.df, by=c('id'='parcours')) %>%
#    filter(pdf=='oui') %>%
    glimpse()
  cartesDir <- sprintf("%s/cartes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
#
# le template tex
  dsn <- sprintf("%s/cartes_points_cartes_tpl.tex", texDir)
  template <- readLines(dsn)
  texFic <- sprintf("%s/%s", texDir, "cartes_points_cartes.tex")
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
  df <- nc %>%
    st_set_geometry(NULL)
  for ( i in 1:nrow(nc) ) {
    if ( i > 4 ) {
#      break
    }
    sf1 <- nc[i,]
    cartes_point_carte(sf1, 'gb_photo', lwd=3, marge=300, test=test, alpha=120)
    tpl <- template
    tpl <- tex_df2tpl(df, i, tpl)
    tex <- append(tex, tpl)
  }
  write(tex, file = TEX, append=FALSE)
}
#
# génération d'une carte
cartes_point_carte <- function(nc, couche='sc1000', lwd=3, test=2, marge=0, alpha=120) {
  library(raster)
  cartesDir <- sprintf("%s/cartes", texDir)
  parcours <- sprintf('%s_%s', nc$id[1], nc$point[1])
  dsn <- sprintf('%s/%s_%s.pdf', cartesDir, parcours, couche)
  if (file.exists(dsn) & test>1) {
    return()
  }
  carp('dsn: %s', dsn)
#  return()
  rasterFic <- sprintf('%s/ogc/%s_%s.tif', varDir, parcours, couche)
  carp("rasterFic: %s", rasterFic)
#  stop('***')
  img <- brick(rasterFic)
  if ( marge > 0 ) {
    e <- fonds_extent(nc, marge)
    img <- crop(img, e)
  }
#  img1 <- projectRaster(img, crs=CRS("+init=epsg:2154"))
  plotImg(img, alpha=alpha)
  text(st_coordinates(st_centroid(nc)), labels = nc$point, cex=2, col='black')
  nc1 <- st_buffer(nc, 100)
  plot(st_geometry(nc1), add=TRUE, border='black', lwd=3)
  nc1 <- st_buffer(nc, 200)
  plot(st_geometry(nc1), add=TRUE, border='black', lwd=3)
  scalebar(200, type='bar', divs=2)
  dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
  graphics.off()
}
#
# la découpe du pdf par observateur
# source("geo/scripts/onc35.R");cartes_points_pdf();
cartes_points_pdf <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  parcours.df <- drive_parcours_lire()
  nb_pages <- 4
  df <- ppe_points_lire() %>%
    left_join(parcours.df, by=c('id'='parcours')) %>%
#    filter(pdf=='oui') %>%
    st_set_geometry(NULL) %>%
    group_by(carre, prenom) %>%
    summarize(nb=n()) %>%
    print(n=20) %>%
    ungroup() %>%
    mutate(fichier=sprintf("%s_points", carre)) %>%
    mutate(id=1:nrow(.), id=id-1) %>%
    mutate(premiere=id*nb_pages+1) %>%
    mutate(derniere=(id+1)*nb_pages) %>%
    glimpse() %>%
    dplyr::select(dossier=prenom, fichier, premiere, derniere) %>%
    glimpse()
#  stop('***')
  sedja_decoupe_sh(dossier='ONC35', fichier='points', df)
}
