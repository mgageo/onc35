# <!-- coding: utf-8 -->
#
# quelques fonctions pour les Oiseaux Nicheurs Communs 35
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# https://workshops.distancesampling.org/stand-intermed-2018/R_tutorial/1-1_Intro_to_R_basics_solutions.pdf
#
# !!!!
# remplacement de couches_donnees_lire
# par ppe_donnees_lire
# !!!!!!!!!!!!!!!!!!!!!!!!
# source("geo/scripts/onc35.R");ds_jour()
ds_jour <- function() {
  library(sf)
  library(tidyverse)
  ds_donnees_points_ds_ecrire()
}
#
# les données "parcours" format "DistanceSampling"
# source("geo/scripts/onc35.R"); ds_donnees_parcours_ds_ecrire()
ds_donnees_parcours_ds_ecrire <- function(test=1) {
  carp()
  library(tidyverse)
#
# le répertoire du logiciel
  dsDir <<- 'C:/Users/Marc/Documents/My Distance Projects'
#  dsDir <- sprintf("%s/distance", texDir)
  dir.create(dsDir, showWarnings = FALSE)
  nc <- couches_donnees_lire()
  donnees_extraire(nc)
  donnees.df <- donnees.sf %>%
    st_set_geometry(NULL)
  df <- donnees.df %>%
    filter(detection=='oreille') %>%
    glimpse()
  especes.df <- df %>%
    group_by(espece) %>%
    summarize(nb=n(), nombre=sum(nombre)) %>%
    filter(nb>120) %>%
    glimpse()
  especes.df$camel <- camel5(especes.df$espece)
  df <- df %>%
    filter(espece %in% especes.df$espece) %>%
    glimpse()
# l'effort par parcours
  parcours.df <- donnees_parcours_effort(donnees.df) %>%
    st_set_geometry(NULL)
  for ( i in 1:nrow(especes.df) ) {
    esp <- as.character(especes.df[i, 'espece'])
    camel <- as.character(especes.df[i, 'camel'])
    carp('espece: %s %s', esp, camel)
    esp.df <- df %>%
      filter(espece == !!esp) %>%
      dplyr::select(carre, distance) %>%
      left_join(parcours.df) %>%
      glimpse()
    carp("les parcours sans données")
    p1.df <- parcours.df %>%
      filter(! label %in% esp.df$label) %>%
      mutate(distance='') %>%
      glimpse()
    esp.df <- rbind(esp.df, p1.df) %>%
      mutate(Region='ONC35', Surface=100) %>%
      dplyr::select(Region, Surface, transect=carre, longueur, distance) %>%
      glimpse()
    dsn <- sprintf("%s/%s_parcours.txt", dsDir, camel)
    write.table(esp.df, file=dsn, quote=FALSE, sep="\t", eol="\n", col.names=T, row.names=F)
#    stop('***')
  }
}
#
# les données "point d'écoute" format "DistanceSampling"
# source("geo/scripts/onc35.R"); ds_donnees_points_ds_ecrire()
ds_donnees_points_ds_ecrire <- function(test=1) {
  carp()
  library(tidyverse)
#
# le répertoire du logiciel
  dsDir <<- 'C:/Users/Marc/Documents/My Distance Projects'
#  dsDir <- sprintf("%s/distance", texDir)
  dir.create(dsDir, showWarnings = FALSE)
  nc <- ppe_donnees_lire()
  donnees_extraire(nc)
  donnees.df <- donnees.sf %>%
    st_set_geometry(NULL)
  df <- donnees.df %>%
    filter(type=='point') %>%
    filter(detection=='oreille') %>%
    glimpse()
  especes.df <- df %>%
    group_by(espece) %>%
    summarize(nb=n(), nombre=sum(nombre)) %>%
    filter(nb>30) %>%
    glimpse()
  especes.df$camel <- camel5(especes.df$espece)
  df <- df %>%
    filter(espece %in% especes.df$espece) %>%
    glimpse()
# l'effort par points
  points.df <- donnees.df %>%
    filter(type=='point') %>%
    group_by(label, session) %>%
    summarize(nb=n()) %>%
    group_by(label) %>%
    summarize(Effort=n()) %>%
    glimpse()
  for ( i in 1:nrow(especes.df) ) {
    esp <- as.character(especes.df[i, 'espece'])
    camel <- as.character(especes.df[i, 'camel'])
    carp('espece: %s %s', esp, camel)
    esp.df <- df %>%
      filter(espece == !!esp) %>%
      dplyr::select(label, distance) %>%
      left_join(points.df) %>%
      glimpse()
    carp("les points sans données")
    p1.df <- points.df %>%
      filter(! label %in% esp.df$label) %>%
      mutate(distance='') %>%
      glimpse()
    esp.df <- rbind(esp.df, p1.df) %>%
      mutate(Region='ONC35', Surface=100*10) %>%
      dplyr::select(Region, Surface, point=label, Effort, distance) %>%
      glimpse()
    dsn <- sprintf("%s/%s.txt", dsDir, camel)
    write.table(esp.df, file=dsn, quote=FALSE, sep="\t", eol="\n", col.names=T, row.names=F)
    carp("dsn: %s", dsn)
    ds.df <- ds_ds_detection(as.data.frame(esp.df)) %>%
      glimpse()
    dsn <- sprintf("%s/especes/%s_ds.pdf", texDir, camel)
    dev2pdf(dsn=dsn, w=8, h=4)
  }
}
#
# les données pour les espèces en format "DistanceSampling"
# rm(list=ls()); source("geo/scripts/onc35.R");ds_donnees_points()
ds_donnees_points <- function(test=1) {
  library(tidyverse)
  library(utf8)
  library(janitor)
  library(ggplot2)
  library(hrbrthemes)
  library(viridis)
#
# le répertoire du logiciel
#  dsDir <- 'C:/Users/Marc/Documents/My Distance Projects'
  dsDir <- sprintf("%s/distance/", texDir)
  carp('dsDir: %s', dsDir)
  files <- list.files(dsDir, pattern = "Pins.*txt$", full.names = TRUE, ignore.case = TRUE, recursive = FALSE)
  for (i in 1:length(files)) {
    file <- files[i]
    carp('file: %s', file)
    df <- read_delim(file, delim="\t", col_names=TRUE)
    ds.df <- ds_ds_detection(as.data.frame(df)) %>%
      glimpse()
  }
}
#
# utilisation du package Distance
ds_ds_detection <- function(df) {
  library(Distance)
  df <- df %>%
    mutate(distance=as.numeric(distance)) %>%
    drop_na(distance) %>%
    glimpse()
  cutpts <- seq(0, 250, 50)
  dmax <- 250
  dmax <- max(df$distance)
  carp('dmax: %s', dmax)
  cutpts <- seq(0, dmax, by=dmax/6)
  df_ht <- ds(df, truncation=dmax, transect="point", formula=~1, key="hn", adjustment=NULL, cutpoints=cutpts)
  plot(df_ht)
  stop('***')
  df1 <- AIC(df_ht)
  p <- ds.gof(df_ht)
  df1[1, 'chi'] <- p$chisquare$chi1$chisq
  legendes <- sprintf('nb: %d', nrow(df_ht$ddf$data))
  legendes <- sprintf('%s\nchi: %.2f\nAIC: %.0f', legendes, df1[1, 'chi'],  df1[1, 'AIC'])

  text( par( "usr" )[ 2 ], par( "usr" )[ 4 ], legendes,    adj = c( 1, 1 ), col = "blue" )
  return(invisible(df1))
}
#
# utilisation du package unmarked
ds_um_detection <- function(df) {
  library(unmarked)
  cut <- c(0, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250)
  dmax <- 250
  dmax <- max(df$distance)
  cut <- seq(0, dmax, by=dmax/6)
}
#
# estimation de la distance de détection
# source("geo/scripts/onc35.R"); ds_distance_detection()
ds_distance_detection <- function(test=1) {
  carp()
  library(tidyverse)
  library(knitr)
  library(ggplot2)
  nc <- ppe_donnees_lire("2019")
  donnees_extraire(nc)
  donnees.df <- donnees.sf %>%
    st_set_geometry(NULL)
#    filter(point_email %in% c("famillebeaufils@wanadoo.fr", "nicolashyon@free.fr", "demarquet.quentin@gmail.com", "regis.morel@bretagne-vivante.org", "maroquesnej@gmail.com", "joel.lamour@hotmail.fr"))
  df <- donnees.df %>%
    glimpse()
  especes.df <- df %>%
    group_by(espece) %>%
    summarize(nb=n(), nombre=sum(nombre)) %>%
    filter(nb > 10) %>%
    glimpse()
  especes.df$camel <- camel5(especes.df$espece)
  df <- df %>%
    filter(espece %in% especes.df$espece) %>%
    glimpse()
  pdfDir <<- sprintf("%s/histo", texDir)
  if( ! file.exists(pdfDir) ) {
    dir.create(pdfDir)
  }
#
# le template tex
  dsn <- sprintf("%s/ds_distance_detection_especes_tpl.tex", texDir)
  template <- readLines(dsn, encoding = "UTF-8")
  template <- iconv(template, to = "UTF-8")
  texFic <- sprintf("%s/%s", texDir, "ds_distance_detection_especes.tex")
  dir.create(pdfDir, showWarnings = FALSE)
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
  for ( i in 1:nrow(especes.df) ) {
    tpl <- template
    tpl <- tex_df2tpl(especes.df, i, tpl)
    tex <- append(tex, tpl)
    esp <- especes.df[[i, 'espece']]
    camel <- especes.df[[i, 'camel']]
    carp('espece: %s %s', esp, camel)
    esp.df <<- df %>%
      filter(espece == !!esp)
    breaks <- quantile(esp.df$distance, probs = c (0.85), na.rm = T)
    distance <- breaks[[1]]
    especes.df[i, "distance"] <- distance
    x_max <- max(esp.df$distance)
    x <- ceiling(x_max / 10) * 10
    breaks <- seq(0, x, by = 10)
#    cuts <- cut(esp.df$distance, breaks=breaks, include.lowest = TRUE) %>%
#      glimpse()
    titre <- sprintf("%s nb: %s dd: %s", esp, nrow(esp.df), distance)
    gg <- ggplot(data = esp.df, aes(x = distance)) +
      geom_histogram(breaks = breaks) +
      geom_vline(xintercept = distance, linetype = "dashed", color = "red", size = 2) +
      ggtitle(titre)
    print(gg)
#    stop("****")
    dsn <- sprintf("%s/%s.pdf", pdfDir, camel)
    ggsave(dsn, plot=gg)
  }
  especes.df <- especes.df %>%
    dplyr::select(-nombre, -camel)
  print(kable(especes.df))
  write(tex, file = TEX, append = FALSE)
  carp("texFic: %s", texFic)
  tex_pdflatex("ds_distance_detection.tex")
}
ds_distance_detection_espece <- function(df) {
  carp()
  library(tidyverse)
  library(ggplot2)
  breaks <- quantile(df$distance, probs = c (0.85), na.rm = T)
  distance <- breaks[[1]]
  carp("distance: %s", distance)
#  stop("****")
}