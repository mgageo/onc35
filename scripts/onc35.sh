#!/bin/sh
# <!-- coding: utf-8 -->
#T onc35 : monitoring des oiseaux
# auteur: Marc Gauthier
[ -f ../win32/scripts/misc.sh ] && . ../win32/scripts/misc.sh
[ -f ../win32/scripts/mga.sh ] && . ../win32/scripts/mga.sh
#f CONF:
CONF() {
  LOG "CONF debut"
  CFG="ONC35"
  [ -d "${CFG}" ] || mkdir "${CFG}"
  LOG "CONF fin"
}
#F GIT: pour mettre à jour le dépot git
GIT() {
  LOG "GIT debut"
  Local="${DRIVE}/web/geo";  Depot=onc35; Remote=frama
  export Local
  export Depot
  export Remote
  _git_lst
  bash ../win32/scripts/git.sh INIT $*
#  bash ../win32/scripts/git.sh PUSH
  remoteDir="${DRIVE}/web.git/${Remote}/${Depot}"
#  cd $remoteDir
#  pwd
#  git rm -f -r scripts
#  git commit -m "done"
  LOG "GIT fin"
}
#f _git_lst: la liste des fichiers pour le dépot
_git_lst() {
  cat  <<'EOF' > /tmp/_git.lst
scripts/onc35.sh
EOF
#  ls -1 scripts/onc35*.R >> /tmp/git.lst
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/onc35.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/oncb.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/ds.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  cat /tmp/_git.lst > /tmp/git.lst
#  exit
#  ls -1 ${CFG}/*.tex >> /tmp/git.lst
  cat  <<'EOF' > /tmp/README.md
# onc35 : traitement de données Oiseaux Nicheurs Communs d'I&V
# oncb : traitement de données Oiseaux Nicheurs Communs de Bretagne
# ds : Distance Sampling
Scripts en environnement Windows 10 : MinGW R

Ces scripts exploitent des données en provenance des bases Biolovision.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

EOF
}
[ $# -eq 0 ] && ( HELP )
CONF
while [ "$1" != "" ]; do
  case $1 in
    -c | --conf )
      shift
      Conf=$1
      ;;
    * )
      $*
      exit 1
  esac
  shift
done