# <!-- coding: utf-8 -->
#
# quelques fonctions pour les Oiseaux Nicheurs Communs 35
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# source("geo/scripts/onc35.R");especes_jour()
especes_jour <- function() {
  library(sf)
  library(tidyverse)
  especes_donnees_tex()
}
#
# répartition par session
# source("geo/scripts/onc35.R"); especes_session()
especes_sessions <- function(test=1) {
  carp()
  library(tidyverse)
  library(utf8)
  library(janitor)
  library(ggplot2)
  library(hrbrthemes)
  library(viridis)
  especesDir <- sprintf("%s/especes", texDir)
  dir.create(especesDir, showWarnings = FALSE)
  nc <- couches_donnees_lire()
  donnees_extraire(nc)
  donnees.df <- donnees.sf %>%
    st_set_geometry(NULL) %>%
    glimpse()
  especes.df <- donnees.df %>%
    group_by(espece) %>%
    summarize(nb=n(), nombre=sum(nombre)) %>%
    filter(nb>10) %>%
    glimpse()
  df <- donnees.df %>%
#    filter(espece %in% especes.df$espece) %>%
    group_by(espece, session) %>%
    summarize(nb=n()) %>%
    spread(key=session, value=nb, fill=0) %>%
    adorn_totals(where = c("col")) %>%
    mutate(pc=Total/sum(Total)) %>%
    adorn_totals(where = c("row")) %>%
    mutate(pc=round_half_up(pc*100,digits=1))
  View(df)
  tex_df2table(df)
  tex_pdflatex('especes.tex')
}
#
# répartition spatiale
# source("geo/scripts/onc35.R"); especes_spatiale()
# https://stackoverflow.com/questions/48219732/pass-a-string-as-variable-name-in-dplyrfilter
especes_spatiale <- function(test=1) {
  carp()
  library(tidyverse)
  library(utf8)
  library(janitor)
  library(ggplot2)
  library(hrbrthemes)
  library(viridis)
  especesDir <- sprintf("%s/especes", texDir)
  dir.create(especesDir, showWarnings = FALSE)
  if (!  exists('especes_donnees.df') ) {
    nc <- couches_donnees_lire()
    donnees_extraire(nc)
    donnees.df <- donnees.sf %>%
      st_set_geometry(NULL) %>%
      glimpse()
    especes_donnees.df <<- donnees.df
  } else {
    donnees.df <- especes_donnees.df
  }
  groupe <- 'carre'
  df <- donnees.df %>%
    mutate(valeur=sprintf('%s_%s', carre, session))
  df1 <- df %>%
    group_by(valeur) %>%
    summarize(nb=n())
  total <- df[1, 'nb']
  df2 <- df %>%
    group_by(espece, valeur) %>%
    summarize(nb=n()) %>%
    group_by(espece) %>%
    summarize(nb=n()) %>%
    mutate(effort=!!as.name(total)) %>%
    glimpse()
  View(df2)
#  tex_df2table(df)
#  tex_pdflatex('especes.tex')
}
#
# les données pour les espèces majoritaires, la version Tex
# source("geo/scripts/onc35.R"); especes_donnees_tex()
especes_donnees_tex <- function(test=1) {
  carp()
  library(tidyverse)
  library(utf8)
  library(janitor)
  library(ggplot2)
  library(hrbrthemes)
  library(viridis)
  texFic <- sprintf("%s/%s", texDir, "especes_donnees.tex")
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
  especesDir <- sprintf("%s/especes", texDir)
  dir.create(especesDir, showWarnings = FALSE)
# http://www.tablesgenerator.com/
#
# le template tex
  dsn <- sprintf("%s/donnees_espece_tpl.tex", texDir)
  template <- readLines(dsn, encoding="UTF-8")
  template <- iconv(template, to='UTF-8')

  nc <- couches_donnees_lire()
  donnees_extraire(nc)
  donnees.df <- donnees.sf %>%
    st_set_geometry(NULL) %>%
    glimpse()
  especes.df <- donnees.df %>%
    group_by(espece) %>%
    summarize(nb=n(), nombre=sum(nombre)) %>%
    filter(nb>100) %>%
    glimpse()
#
# les différents chiffres
#
# l'effort par points
  points.df <- donnees.df %>%
    filter(type=='point') %>%
    group_by(label, session) %>%
    summarize(nb=n()) %>%
    group_by(label) %>%
    summarize(Effort=n()) %>%
    glimpse()
  points_nb <- nrow(points.df)
  points_effort <- sum(points.df$Effort)
  carp("points_nb %d %d", points_nb, points_effort)
# l'effort par parcours
  parcours.sf <- donnees_parcours_effort(donnees.df)
  parcours_nb <- nrow(parcours.sf)
  parcours_effort <- sum(parcours.sf$Effort)
  parcours_longueur <- sum(parcours.sf$longueur)
  carp("parcours_nb %d %d %d", parcours_nb, parcours_effort, parcours_longueur)
#
# pour les carrés
  df <- donnees.df
  df$champ <- df$carre
  nb.df <- df %>%
    group_by(espece, champ) %>%
    summarize(nb=n(), nombre=sum(nombre)) %>%
    group_by(espece) %>%
    summarize(nb_carres=n()) %>%
    glimpse()
  especes.df <- especes.df %>%
    left_join(nb.df)
#
# pour les points
  df <- donnees.df %>%
    filter(type=='point')
  df$champ <- df$label
  nb.df <- df %>%
    group_by(espece, champ) %>%
    summarize(nb=n(), nombre=sum(nombre)) %>%
    group_by(espece) %>%
    summarize(nb_points=n()) %>%
    glimpse()
  especes.df <- especes.df %>%
    left_join(nb.df)
# pour les parcours
  carp('parcours')
  df <- donnees.df %>%
    filter(type=='parcours') %>%
    glimpse()

  df$champ <- df$label
  nb.df <- df %>%
    group_by(espece, champ) %>%
    summarize(nb=n(), nombre=sum(nombre)) %>%
    group_by(espece) %>%
    summarize(nb_parcours=n()) %>%
    glimpse()
  especes.df <- especes.df %>%
    left_join(nb.df)
#
# les distances
  df <- donnees.df %>%
    group_by(espece) %>%
    summarize(dmin=min(distance),
              dmoy=as.integer(mean(distance)),
              dmed=as.integer(median(distance)),
              dmax=max(distance),
              dvar=var(distance),
              dsd=sd(distance)
            ) %>%
    glimpse()
  especes.df <- especes.df %>%
    left_join(df)
#
# la détection
  df <- donnees.df %>%
    group_by(espece, detection) %>%
    summarize(nb=n()) %>%
    spread(key=detection, value=nb, fill=0)
  especes.df <- especes.df %>%
    left_join(df)
#  stop("***")
#  test <- 2
  especes.df$camel <- camel5(especes.df$espece)
  for ( i in 1:nrow(especes.df) ) {
    if ( i > 4 ) {
#      break
    }
#    carresDir <- sprintf("%s/especes/%s", texDir, especes.df[i, 'espece'])
#    dir.create(carresDir, showWarnings = FALSE, recursive = TRUE)
    esp <- as.character(especes.df[i, 'espece'])
    camel <- as.character(especes.df[i, 'camel'])
    carp('espece: %s %s', esp, camel)
    s.df <- donnees.df %>%
      filter(espece == !!esp) %>%
      dplyr::select(espece, distance) %>%
      glimpse()
    ggplot(s.df, aes(x=distance)) +
      geom_histogram(binwidth=25) +
      theme_bw()
    dsn <- sprintf("%s/especes/%s.pdf", texDir, camel)
    ggsave(dsn, width = 8, height = 4)
    tpl <- template
    tpl <- tex_df2tpl(especes.df, i, tpl)
#
# les points d'écoute
    s.df <- donnees.df %>%
      filter(espece == !!esp) %>%
      filter(detection == 'oreille') %>%
      filter(type == 'point')
#      dplyr::select(espece, distance, nombre) %>%
#      glimpse()
    dmax <- max(s.df$distance)
    n <- nrow(s.df)
    cutpts <- seq(0, dmax, by=dmax/6)
    h <- cut(s.df$distance, cutpts)
    N <- max(table(h)) * length(table(h))
    phat <- sprintf('%.03f', n/N)
    area <- (dmax*dmax*3.14159)/10000
    surface <- sprintf('%.0f', area)
    densite <- sprintf('%.0f', (((N/points_effort)*100)/area))
    bw <- (dmax+1) / 6
    ggplot(s.df, aes(x=distance)) +
      geom_histogram(binwidth=bw) +
      theme_bw()
#    stop('***')
    dsn <- sprintf("%s/especes/%s_points.pdf", texDir, camel)
    ggsave(dsn, width = 8, height = 4)
    ds.df <- s.df %>%
      group_by(espece) %>%
      summarize(
              points_donnees=n(),
              points_oiseaux=sum(nombre),
              points_dmin=min(distance),
              points_dmoy=as.integer(mean(distance)),
              points_dmed=as.integer(median(distance)),
              points_dmax=max(distance),
              points_dvar=var(distance),
              points_dsd=sd(distance)
            ) %>%
      glimpse()
    ds.df[1, 'points_phat'] <- phat
    ds.df[1, 'points_N'] <- N
    ds.df[1, 'points_surface'] <- surface
    ds.df[1, 'points_densite'] <- densite
    ds.df[1, 'points_nb'] <- points_nb
    ds.df[1, 'points_effort'] <- points_effort
    tpl <- tex_df2tpl(ds.df, 1, tpl)
#
# les parcours et les points d'écoute
    s.df <- donnees.df %>%
      filter(espece == !!esp) %>%
      filter(detection == 'oreille')
#      dplyr::select(espece, distance, nombre) %>%
#      glimpse()
# calcul simpliste de la densité
    dmax <- max(s.df$distance)
    n <- nrow(s.df)
    cutpts <- seq(0, dmax, by=dmax/6)
    h <- cut(s.df$distance, cutpts)
    N <- max(table(h)) * length(table(h))
    dmax <- max(s.df$distance)
    n <- nrow(s.df)
    cutpts <- seq(0, dmax, by=dmax/6)
    h <- cut(s.df$distance, cutpts)
    N <- max(table(h)) * length(table(h))
    area <- (dmax* parcours_longueur)/10000
    surface <- sprintf('%.0f', area)
    densite <- sprintf('%.0f', ((N*100)/area))

    bw <- dmax / 6
    ggplot(s.df, aes(x=distance)) +
      geom_histogram(binwidth=bw) +
      theme_bw()
#    stop('***')
    ds.df <- s.df %>%
      group_by(espece) %>%
      summarize(
              parcours_donnees=n(),
              parcours_oiseaux=sum(nombre),
              parcours_dmin=min(distance),
              parcours_dmoy=as.integer(mean(distance)),
              parcours_dmed=as.integer(median(distance)),
              parcours_dmax=max(distance),
              parcours_dvar=var(distance),
              parcours_dsd=sd(distance)
            ) %>%
      glimpse()
    ds.df[1, 'parcours_nb'] <- parcours_nb
    ds.df[1, 'parcours_effort'] <- parcours_effort
    ds.df[1, 'parcours_longueur'] <- parcours_longueur
    ds.df[1, 'parcours_N'] <- N
    ds.df[1, 'parcours_surface'] <- surface
    ds.df[1, 'parcours_densite'] <- densite
    tpl <- tex_df2tpl(ds.df, 1, tpl)
    dsn <- sprintf("%s/especes/%s_parcours.pdf", texDir, camel)
    ggsave(dsn, width = 8, height = 4)

 #   stop('***')

    tex <- append(tex, tpl)

  }
  write(tex, file = TEX, append = FALSE)
  carp("texFic: %s", texFic)
  tex_pdflatex('especes.tex')
}
