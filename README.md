# onc35 : traitement de données Oiseaux Nicheurs Communs d'I&V
# oncb : traitement de données Oiseaux Nicheurs Communs de Bretagne
# ds : Distance Sampling
Scripts en environnement Windows 10 : MinGW R

Ces scripts exploitent des données en provenance des bases Biolovision.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

